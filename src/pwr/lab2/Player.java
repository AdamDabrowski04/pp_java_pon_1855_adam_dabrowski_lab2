package pwr.lab2;

import org.omg.Messaging.SYNC_WITH_TRANSPORT;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Vector;

/**
 * Created by Adam on 2015-05-10.
 */
public class Player {
    String name;
    int wins=0;
    int bet=0;
    int gamesPlayed =0;
    private int money=0;
    private Vector<Card> cards;

    public Player(String name, int money)
    {
        cards =new Vector<Card>();
        this.name=name;
        this.money=money;
    }
    public void win()
    {
        System.out.println(name + " won " + 2 * bet);
        money+=2*bet;
        bet=0;
        wins++;
        gamesPlayed++;
        cards.removeAllElements();

    }
    public  void lose()
    {
        System.out.print(name+" lost "+bet);
        money-=bet;
        gamesPlayed++;
        bet =0;
        cards.removeAllElements();
    }

    public void removeAllCards()
    {
        cards.removeAllElements();
    }
    public int getAvilableMoney()
    {
        return money;
    }
    public void addCard(Card card)
    {
        cards.add(card);
    }
    public int handValue()
    {
        int sum=0;
        for(Card card : cards)
        {
            sum+=card.figure.value;
        }
        return sum;
    }
    public int printHand()
    {
        System.out.println("****************************************");
        System.out.println(name);
        for(Card card : cards)
        {
           System.out.println(card.figure+" of "+card.color);
        }
        System.out.println("****************************************");
        return this.handValue();
    }
    public void makeBet() {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str;

        try {
            do {
                System.out.println(name+ " how much do you want to bet (write number between 1 and " + money + "): ");
                str = br.readLine();
                bet = Integer.valueOf(str);
                if (bet > money || bet < 1) {
                    System.out.println("Invalid number please try again.");
                }

            } while (bet > money || bet < 1);
        } catch (IOException e) {
            System.out.println("Trouble reading answer!" + e);
        }
    }
    public void compareHands(int croupierHand)
    {
        if(handValue()==21)
        {
            win();
            return;
        }
        if(handValue()>21)
        {
            lose();
            return;
        }
        if(croupierHand>21)
        {
            win();
            return;
        }
        if(handValue()>croupierHand)
        {
            win();
            return;
        }
        else
        {
            lose();
        }
    }
}
