package pwr.lab2;

import sun.text.resources.ro.CollationData_ro;

/**
 * Created by Adam on 2015-05-10.
 */
enum Color
{
    spades(1), clubs(2), hearts(3), diamonds(4);

    int val;
    Color(int val)
    {
        this.val=val;
    }

}

enum Figure
{
    two(2), three(3), four(4), five(5), six(6), seven(7), eight(8), nine(9), ten(10),
    knave(10), queen(10), king(10),ace(11);

    int value;
    Figure(int value)
    {
        this.value=value;

    }
}

public class Card {
    Color color;
    Figure figure;
    int numerOfCards=0; // liczba kart danego typu
    public Card(Color color, Figure figure)
    {
        this.color=color;
        this.figure=figure;
    }
}
