package pwr.lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.IntSummaryStatistics;
import java.util.Vector;

/**
 * Created by Adam on 2015-05-10.
 */
public class BlackJack {
    Player croupier;
    Vector<Player> players;
    Deck deck;
    int allGames=0;
    public BlackJack()
    {
        players= new Vector<Player>();
        deck = new Deck();
        deck.addDeck();
        deck.addDeck();
        croupier = new Player("Croupier", Integer.MAX_VALUE);

    }
    public void addPlayer(String name, int money)
    {
        players.add(new Player(name, money));

    }

    public void singleGame()
    {
        String tmpString;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        gamePreparation();
        for(Player player:players)
        {
            player.addCard(deck.pullRandomCard());
            player.addCard(deck.pullRandomCard());
            player.printHand();
            player.makeBet();
            try
            {
                do
                {
                    System.out.print("1-HIT ME 2-PAS: ");
                    tmpString=br.readLine();
                    if (Integer.valueOf(tmpString)==1)
                    {
                        player.addCard(deck.pullRandomCard());
                    }
                    player.printHand();
                }while(Integer.valueOf(tmpString)!=2&&player.handValue()<21);
            }
            catch(IOException e)
            {
                System.out.println("You is stupid "+e);
            }
        }
        while(croupier.handValue()<=16)
        {
            croupier.addCard(deck.pullRandomCard());
        }
        croupier.printHand();
        for(int i=0;i<players.size();i++)
        {
            players.elementAt(i).compareHands(croupier.handValue());
            if (players.elementAt(i).getAvilableMoney()==0)
            {
                System.out.println("I am sorry, but "+players.elementAt(i).name+" lost all his/her money and is removed from the table by angry guards");
                players.remove(i);
            }

        }
        croupier.removeAllCards();

    }
    public void doYouWantToPlayAGame()
    {
        while(players.size()>0)
        {
            System.out.println("\n");
            allGames++;
            singleGame();
            if (allGames/52<1)
            {
                deck.addDeck();
            }
        }

    }

    private void gamePreparation()
    {
        Card card;
        croupier.addCard(deck.pullRandomCard());
        card=deck.pullRandomCard();
        croupier.addCard(card);
        System.out.println("Croupier got an unknown card and "+ card.figure + " of "+card.color);

    }
}
