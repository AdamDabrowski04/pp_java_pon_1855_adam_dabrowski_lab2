package pwr.lab2;
import pwr.lab2.Color;
import pwr.lab2.Figure;

import javax.xml.ws.soap.Addressing;
import java.util.Random;
import java.util.Vector;

/**
 * Created by Adam on 2015-05-10.
 */
public class Deck {

    Vector<Card> deck;
    int numberOfCardsInDeck=0;
    Random generator = new Random();

    public Deck()
    {
        deck = new Vector<Card>();
        for(Color color:Color.values())
        {
            for(Figure figure: Figure.values())
            {
                deck.add(new Card(color, figure));
            }
        }
    }
   public void addDeck()
   {
       for(Card card : deck)
       {
           card.numerOfCards+=4;
       }
       numberOfCardsInDeck+=52;
   }

    public Card pullRandomCard()
    {
        if(numberOfCardsInDeck<=0)
        {
            throw new NullPointerException("No cards in deck");
        }
        int pickedCardIndex=0;
        int pickedCardProbability = generator.nextInt()+deck.elementAt(0).numerOfCards;
        for(int i =1; i<deck.size();i++)
        {
            int test=generator.nextInt()+deck.elementAt(i).numerOfCards;
            if(test>pickedCardProbability&&deck.elementAt(i).numerOfCards>0)
            {
                pickedCardIndex=i;
                pickedCardProbability=test;
            }
        }
        numberOfCardsInDeck--;
        deck.elementAt(pickedCardIndex).numerOfCards-=1;
        return deck.elementAt(pickedCardIndex);
    }

}
